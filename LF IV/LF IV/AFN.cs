﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LF_IV
{
    class AFN
    {
        private List<string> vocabulary;

        private List<Rule> nod;

        private string word;

        private string initialNode;
        private string finalNode;
        private bool failed = false;

        #region Properties

        public List<string> Vocabulary
        {
            get { return vocabulary; }
            set { vocabulary = value; }
        }

        public List<Rule> Nodes
        {
            get { return nod; }
            set { nod = value; }
        }

        public string Word
        {
            get { return word; }
            set { word = value; }
        }


        #endregion

        public AFN()
        {
            Initialize();
             
            ExecuteAlgorithm();
        }
        #region Init
        private void Initialize()
        {
            AssignVocabulary();

            AssignWord();

            AssignRules();
        }

        private void AssignWord()
        {
            Word = Read.Storage[1].FirstOrDefault();
        }

        private void AssignVocabulary()
        {
            Vocabulary = new List<string>();
            Read.Storage[0].FirstOrDefault().ToCharArray().ToList<char>().ForEach(n =>
            {

                if (n != ',' && n != ' ')
                    Vocabulary.Add(n.ToString());
            });
        }

        private void AssignRules()
        {

            int poz = 0;
            nod = new List<Rule>();

            Read.Storage[2].ForEach(n =>
            {
                nod.Add(new Rule(AddState(n, poz++), AddDirections(n)));
            });
        }

        #endregion
        private Dictionary<string, List<string>> AddDirections(string s)
        {
            var dex = new Dictionary<string, List<string>>();

            var directions = Regex.Matches(s, @"\s*[{}\d,]+");
            
            for (int i = 1; i < directions.Count; i++)
            {
                dex.Add(Vocabulary[i-1], GetDirections(directions[i].Value));
            }

            return dex;
        }
        
        private List<string> GetDirections(string value)
        {
            return Regex.Matches(value, @"[0-9-]").Cast<Match>().Select(n => n.Value).ToList();
        }

        //poz - used to determine position of initialNode
        private string AddState(string element, int poz)
        {
            var state = Regex.Match(element, @"[a-z]+").Value;
            if(state == "initial")
            {
                initialNode = poz.ToString();
            }
            return state;
        }
        
        private void ExecuteAlgorithm()
        {
            if (!ExecuteRecursion(initialNode, word))
                Console.WriteLine("Failed none of the paths lead to a final node!");
        }
        private Boolean ExecuteRecursion(string destination,string  wword)
        {
            var destinations = nod[Convert.ToInt32(destination)].Directions[wword[0].ToString()];

            wword = wword.Substring(1);//remove first letter

            if (wword == string.Empty || wword == null)
            //in case destinations is of type {0,0,...,0}
               return destinations.Any( n=> CheckResult(n));
            


            foreach(var element in destinations)
            {
                if(element!="-" && ExecuteRecursion(element, wword))
                    return true;
            }

            return false;
        }

        private Boolean CheckResult(string currentNode)
        {
            
            if (nod[Convert.ToInt32(currentNode)].State == "final")
            {
                Console.WriteLine(currentNode + Environment.NewLine + "Correct it is a final state");
                return true;
            }

            return false;
              
        }
    }
}