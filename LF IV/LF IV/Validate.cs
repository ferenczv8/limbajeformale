﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LF_IV
{
    class Validate
    {
        private bool failed = false;

        public bool Failed
        {
            get { return failed; }
            set { failed = value; }
        }


        public void Start()
        {
            ValidateWord();

            ValidateRules();
        }

        //check to seee if word has all characters from the Vocabulary
        private void ValidateWord()
        {

            var letters = Read.Storage[1].FirstOrDefault().ToCharArray().ToList<char>();

            Failed = letters.Any(n =>
            {
                if (!Read.Storage[0].FirstOrDefault().Contains(n.ToString()))
                    return true;
                return false;
            });

            if (Failed == true)
                Console.WriteLine("Word is incorrect");
        }


        private void ValidateRules()
        {
            int count = Read.Storage[2].Count;
            Failed = Read.Storage[2].Any(n =>
            {
                if (!CheckRule(n, count))
                    return true;
                return false;
            });
        }

        private Boolean CheckRule(string s, int max)
        {
            //check format of input for rules 0,{0}
            if (!Regex.Match(s, @"\d\s*,\s*({|)[0-9,-]+(}|),\s*({|)[0-9-,]+(|})\s*((:\s*\w*)|)").Success)
                return false;

            var found = Regex.Matches(s, @"(({|)\d[0-9,]+\d(}|))|(\d)");

            //Check for 3 elements in a rule
            if (found.Count != 3)
                return false;


            //check if there is a Nod that goes to an element that is not defined (-)
            foreach (Match m in found)
            {
                var value = m.Value;
                if (Regex.Match(value, @"(({|)\d[0-9,]+\d(}|))").Success) // if format is like {0,0,... ,0}
                {
                    //TODO: replace with method

                    var bracketFormat = Regex.Matches(value, @"\d");
                    foreach(Match n in bracketFormat)
                    {
                        if (n.Value != "-" && Convert.ToInt32(n.Value) > max)
                            return false;
                    }
                }
                else if (value != "-" && Convert.ToInt32(value) > max)
                    return false;
            }

            return true;
        }
    }
}
