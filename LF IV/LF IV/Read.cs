﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LF_IV
{
    class Read
    {
        private static System.IO.StreamReader file;
        private static List<List<string>> storage;


        public static List<List<string>> StartReading()
        {
            StorageInitialize(); // object where information read is stored

            OpenTextFileReadingStream(); // txt from where I will read all input values

            ReadVocabulary();

            ReadWord();

            ReadNodesAndTheirDirections();

            CloseTextFileReadingStream();

            return storage;
        }

        public static List<List<string>> Storage
        {
            get { return Read.storage; }
        }

        #region Private Methods
        private static void StorageInitialize()
        {
            storage = new List<List<string>>();
        }

        private static void ReadWord()
        {
            Console.WriteLine("Step 2: Read Word");

            var word = ReadFromFile(1);

            storage.Add(word);
        }

        private static void ReadVocabulary()
        {
            Console.WriteLine("Step 1: Reading Vocabulary");

            var vocabulary = ReadFromFile(1); // 1 - number of lines needed to be read to get the vocabulary

            storage.Add(vocabulary);
        }

        private static void ReadNodesAndTheirDirections()
        {
            Console.WriteLine("Step 3 : Reading Nodes");

            Console.WriteLine("Give number of lines - > Keep in mind the values are inserted in text.txt");

            var rules = ReadFromFile(Convert.ToInt32(Console.ReadLine()));

            storage.Add(rules);
        }

        //lineCount - number of lines needed to be read to get the required information
        private static List<string> ReadFromFile(int lineCount)
        {
            int count = 0;
            string line = string.Empty;
            List<string> lines = new List<string>();

            while (count < lineCount && (line = file.ReadLine()) != null)
            {
                lines.Add(line);
                count++;
            }
            return lines;
        }

        private static void OpenTextFileReadingStream()
        {
            string txtpath = System.IO.Directory.GetCurrentDirectory() + "\\Text.txt";

            file = new System.IO.StreamReader(txtpath);
        }

        private static void CloseTextFileReadingStream()
        {
            file.Close();
        }

        #endregion
    }
}
