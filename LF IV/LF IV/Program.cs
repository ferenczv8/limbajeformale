﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LF_IV
{
    public class Program
    {
        private static void StartAFN()
        {
            if (Validate()) //algorithm executes only if input is valid
            { AFN algorithm = new AFN(); } 

        }

        private static Boolean Validate()
        {
            Validate v = new Validate();

            v.Start();

            return !v.Failed;
        }
        public static void Main()
        {
            Read.StartReading();

            StartAFN();
        }
    }
}

