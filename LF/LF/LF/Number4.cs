﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LF
{
    public class Number4
    {
        public void Exercise()
        {
            string[] words = new string[] { "mama", "tataa", "rar" };

            Console.WriteLine(Inversate(LongestWord(words)));
        }

        private string LongestWord(string[] words){

           return words.OrderByDescending(n => n.Length).First(); 
        }
        private string Inversate(string word) {

             return new string (word.Reverse().ToArray());   
        }
    }
}
