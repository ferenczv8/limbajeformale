﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LF
{
    public interface INumber2
    {
        string ValidateFormat(string consoleInput);

        void CheckKeyForDistinct();

        bool TreatTransitivity();

        void ValidateSameKeyValue();
    }
}
