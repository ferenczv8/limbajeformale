﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LF
{
    class Table
    {
        private char _letter;
        private int _index;
        private int _value;

        public Table(int index,char letter)
        {
            _index = index;

            _letter = letter;
        }

        public int Value
        {
            get { return _value; }
            set { _value = value; }
        }
    }
}
