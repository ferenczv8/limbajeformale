﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LF
{
    public class Number3
    {
        private List<Dictionary<char, int>> tab;
        private List<char> col;
        private int lineC;
        private string word;
        public void Exercise()
        {
            FillTable();

            word = Console.ReadLine();

            ValidateWordCol(word);

            FindValue();

        }
        private void FillTable()
        {
            tab = new List<Dictionary<char, int>>();// new Dictionary<int, List<char>>(); 
            string line;
            
            var read = new StreamReader(File.OpenRead("text.txt"));
           
            col = read.ReadLine().Split(' ').SelectMany(x => x.ToCharArray()).ToList<char>();

            
            while ((line = read.ReadLine()) != null)
            {
                int lineCount = 0;
                //List<char> lineValues = line.Split(' ').SelectMany(n => n.ToCharArray()).ToList();
                List<int> lineValues = line.Trim().Split(' ').Select(n => Convert.ToInt32(n) ).ToList();

                Dictionary<char, int> t = new Dictionary<char, int>();

                lineValues.ForEach(n =>
                {
                    t.Add(col[lineCount++], n);
                });

                tab.Add(t);
            }

        }

        private void ValidateWordCol(string word){

           bool  correct = false;

           while(!correct)
           {
               List<char> t = GetWord(word);

                if (correct = t.Any(n => !col.Contains(n)))
                {
                    Console.WriteLine("Insert again a word "); word = Console.ReadLine();
                    correct = false;
                }
                else return;
           }
           
        }
        private List<char> GetWord(string w)
        {
            return Regex.Matches(w, "[a-zA-Z]").Cast<Match>().Select(x => Convert.ToChar(x.Value)).ToList();
        }

        private void FindValue()
         {
            int value = 1;
            GetWord(word).ForEach(n => {
                value = tab[value - 1][n];
            });
            Console.WriteLine(value);
        }
    }
}
