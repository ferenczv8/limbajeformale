﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LF
{
    public class Number5
    {
        public void Exercise()
        {
            string[] num = new string[] { "0","1.2","1.8","100" };

            Console.WriteLine(Calc(num));
        }

        private int Calc(string[] num)
        {
            //int sum = 0;
            //num.ToList().ForEach(n =>
            //{
            //    sum += Convert.ToInt32( Math.Floor( Convert.ToDouble(n)));
            //});
            //return sum;

            return num.ToList().Sum(n => Convert.ToInt32(Math.Floor(Convert.ToDouble(n))));

        }
        
    }
}
