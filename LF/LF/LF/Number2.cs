﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LF
{
    public class Number2 : INumber2
    {
        private Dictionary<char, char> rulesD;
        private List<char> currentRule;
        public Number2() { }
        public void Exercise()
        {
            Console.WriteLine("Insert Word:");
            string word = Console.ReadLine();

            Console.WriteLine("Say how many rules you want");
            int nr = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Insert Rules");  

            ReadRules(nr);
 
            UseRules(word);

            Console.WriteLine(word);
        }

        #region Rules
        private void ReadRules(int nr)
        {
            rulesD = new Dictionary<char, char>();
            
            int i = 0;
            do
            {
                currentRule = GetValues(ValidateFormat(Console.ReadLine()));
               
                if(ValidateRule())
                    rulesD.Add(currentRule[0], currentRule[1]);
                i++;
  
            } while (i < nr);   
        }
        public string ValidateFormat(string input){

            while (!input.Equals(Regex.Match(input, @"[a-zA-Z]->[a-zA-Z]").Value))
            {
                    Console.WriteLine("Sorry -> Input must be of the following format: a->b");
                    input = Console.ReadLine();
            }
                    
            return input;
        }

        private bool ValidateRule()
        {
            ValidateSameKeyValue(); 

            CheckKeyForDistinct(); 

            if (DiferentRoute()) return false;

            if (TreatTransitivity()) return false;

            return true;
        }

        public void CheckKeyForDistinct()
        {
            //bool exist;
            //do
            //{
                //exist = false;
                //rulesD.Keys.ToList().ForEach(x =>
                //{
                //    if (x == currentRule[0])
                //    {
                //        exist = true;
                //        Console.WriteLine("Same key inserted before, please choose another:");
                //        currentRule = GetValues(Console.ReadLine());
                       
                //    }
                //});
                while( rulesD.Keys.Any(x =>
                {
                    if (x == currentRule[0])
                    {
                        Console.WriteLine("Same key inserted before, please choose another:");
                        currentRule = GetValues(ValidateFormat(Console.ReadLine()));
                        return true;
                    }
                    return false; 
                }))  {}
           
        }

        //public bool TreatTransitivity()
        //{
            //bool op = false;
            //rulesD.Values.ToList().ForEach(n => {
            //    if(n == currentRule[0])
            //    {
            //        op = true;
            //        var Keys = rulesD.Where(v => v.Value == n).Select(v => v.Key);

            //        Keys.ToList().ForEach(key => {
            //            if(key == currentRule[1]) // a->b b->a
            //                rulesD.Remove(key);
            //            else
            //                rulesD[key] = currentRule[1]; // a->b b->c => a->c
            //        });

            //        //foreach (var key in Keys)
            //        //{
            //        //    if (key == currentRule[1])
            //        //        rulesD.Remove(key);
            //        //    else
            //        //        rulesD[key] = currentRule[1];
            //        //}
            //    }
            //});
            //return op;
        //}
        public bool TreatTransitivity()
        {
          return rulesD.Values.Any(n => {

                if (n == currentRule[0])
                { 
                    return rulesD.Where(v => v.Value == n).Select(v => v.Key).ToList()

                          .Any(key =>
                          {
                              if (key == currentRule[1]) // a->b b->a
                                  rulesD.Remove(key); 
                              else
                                  rulesD[key] = currentRule[1]; // a->b b->c => a->c
                              return true;
                          });
                }
                return false;
            });
        }

        public void ValidateSameKeyValue()
        {
            while (currentRule[0] == currentRule[1])
            {
                Console.WriteLine("Input must be of the following format: a->b");
                currentRule = GetValues(ValidateFormat(Console.ReadLine()));
            }
        }

        private bool DiferentRoute(){
            //bool op = false;
            //rulesD.Keys.ToList().ForEach(n =>
            //{
            //    if (n == currentRule[1])
            //    {
            //        op = true;
            //        UpdateKey(n, rulesD[n]);
            //    }
            //});
            //return op;

            return rulesD.Keys.Any(n => { 
            
                if(n == currentRule[1])
                {
                    UpdateKey(n, rulesD[n]);
                    return true;
                }
                return false;
            });
        }

        private void UpdateKey(char key,char value)
        {
            rulesD.Remove(key);
            rulesD.Add(currentRule[0], value);
        }

        private List<char> GetValues(string rule){
            return Regex.Matches(rule, @"(?<cap>[a-zA-Z])").Cast<Match>()
                            .Select(x => Convert.ToChar(x.Groups["cap"].Value)).ToList();
        }

        #endregion 

        private void UseRules(string word){

            rulesD.ToList<KeyValuePair<char, char>>().ForEach(n => 
               word = word.Replace(n.Key, n.Value));
        } 
    }
}
