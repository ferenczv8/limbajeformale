﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LF
{
    public class Number1
    {
        public void Exercise()
        {
            
            string prop = "Suntem la ora de limbaje formale";

            Console.WriteLine(wordCount(prop));

            Sort(prop).ForEach(n => Console.WriteLine(n));

        }

        private int wordCount(string sent)
        {
            return sent.Split(' ').ToList().Count();
        }

        private List<string> Sort(string prop)
        {
            var ord = prop.Split(' ').ToList();
            ord.Sort();
            return ord;
        }
    }
}
