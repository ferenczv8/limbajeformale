﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Form1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            
        }

        private void button1_Click(object sender, EventArgs e)
        {
          DialogResult res =  MessageBox.Show("Ex1","Message",MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

          if (res == DialogResult.Yes)
          {
              LF.Number1 t = new LF.Number1();
              t.Exercise();
          }
          

        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show("Ex2", "Message", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);

            if (res == DialogResult.Yes)
            {
                LF.Number2 t = new LF.Number2();
                t.Exercise();
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show("Ex3 Launching", "Message", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);

            if (res == DialogResult.Yes)
            {
                LF.Number3 t = new LF.Number3();
                t.Exercise();
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show("Ex4 Launching", "Message", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);

            if (res == DialogResult.Yes)
            {
                LF.Number4 t = new LF.Number4();
                t.Exercise();
            }

        }

        private void button5_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show("Ex5 Launching", "Message", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);

            if (res == DialogResult.Yes)
            {
                LF.Number5 t = new LF.Number5();
                t.Exercise();
            }
        }
    }
}
