﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LF_II
{
    class Validate
    {
        private List<char> vocab;
        private Dictionary<string, string> rules;
        private string word;
        public Validate() {

        }
        public Validate(StartReading read)
        {
            word = read.Word;

            vocab = read.Vocabulary;

            rules = read.Rules;
        }
        public Dictionary<string,string> ValidateRules()
        {
            int i=0;
            int rulesNumber = rules.Count;
            while(i < rulesNumber){
               var item = rules.ElementAt(i);

               if (RuleExistsInVocabulary(new KeyValuePair<string, string>(item.Key, item.Value)))
                   rulesNumber--; 
               else
                   i++;  // increment only if the rule is not removed from list
            }

            return rules;
        }
        public Boolean ValidateWord()
        {
            return word.ToCharArray().Any(n =>
            {
                if (!vocab.Contains(n))
                   return true;                
                return false;
            });
        }

        //public void CheckKeyForDistinct()
        //{
        //        while( rulesD.Keys.Any(x =>
        //        {
        //            if (x == currentRule[0])
        //            {
        //                Console.WriteLine("Same key inserted before, please choose another:");
        //                currentRule = GetValues(ValidateFormat(Console.ReadLine()));
        //                return true;
        //            }
        //            return false; 
        //        }))  {}
           
        //}   
        //public bool TreatTransitivity()
        //{
        //  return rulesD.Values.Any(n => {

        //        if (n == currentRule[0])
        //        { 
        //            return rulesD.Where(v => v.Value == n).Select(v => v.Key).ToList()

        //                  .Any(key =>
        //                  {
        //                      if (key == currentRule[1]) // a->b b->a
        //                          rulesD.Remove(key); 
        //                      else
        //                          rulesD[key] = currentRule[1]; // a->b b->c => a->c
        //                      return true;
        //                  });
        //        }
        //        return false;
        //    });
        //}

        //public void ValidateSameKeyValue()
        //{
        //    while (currentRule[0] == currentRule[1])
        //    {
        //        Console.WriteLine("Input must be of the following format: a->b");
        //        currentRule = GetValues(ValidateFormat(Console.ReadLine()));
        //    }
        //}

        //private bool DiferentRoute(){
        //    //bool op = false;
        //    //rulesD.Keys.ToList().ForEach(n =>
        //    //{
        //    //    if (n == currentRule[1])
        //    //    {
        //    //        op = true;
        //    //        UpdateKey(n, rulesD[n]);
        //    //    }
        //    //});
        //    //return op;

        //    return rulesD.Keys.Any(n => { 
            
        //        if(n == currentRule[1])
        //        {
        //            UpdateKey(n, rulesD[n]);
        //            return true;
        //        }
        //        return false;
        //    });
        //}

        //private void UpdateKey(char key,char value)
        //{
        //    rulesD.Remove(key);
        //    rulesD.Add(currentRule[0], value);
        //}
        private bool RuleExistsInVocabulary(KeyValuePair<string, string> rule){
            string values = rule.Key + rule.Value;      
            return values.ToCharArray().Where(n => (n != null)).Any(n =>
            {
                if (!vocab.Contains(n) && n!='.')
                {
                    if (n == '@')
                        rules[rule.Key] = string.Empty; // change lamda to string - empty
                    else
                    {
                        rules.Remove(rule.Key);
                        return true;
                    }
                }
                return false;
            });
          
            //var val = values.ToCharArray().Where(n => (n != null)).ToList();

            //foreach (var v in val)
            //{
            //    if (!vocab.Contains(v))
            //    {
            //        rules.Remove(rule.Key);
            //        break;
            //    }
            //}
        }
        public List<string> GetValues(string rule){

            rule = ValidateFormat(rule);

            return rule.Split(new string[] { "->" }, StringSplitOptions.None).ToList();
        }

        private string ValidateFormat(string input)
        {

            while (!input.Equals(Regex.Match(input, @"\S+->\S+").Value))
            {
                Console.WriteLine("Sorry -> Input must be of the following format: string->string");
                input = Console.ReadLine();
            }

            return input;
        }

        
    }
}
