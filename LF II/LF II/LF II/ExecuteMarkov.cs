﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LF_II
{
    static class ExecuteMarkov
    {
        private static Dictionary<string, string> validatedRules;
        private static string word;
        private static bool changed = true;
        static ExecuteMarkov()
        {
            StartReading read = new StartReading();
            word = read.Word;

            Validate rules = new Validate(read);

            validatedRules = rules.ValidateRules();

            if (rules.ValidateWord())
                Console.WriteLine("Word contsins symbols that can not be found in vocabulary");
        }
        public static void Algorithm()
        {
            //write every step into a file.txt
            while (changed)
            {
                changed = false;
                foreach (KeyValuePair<string, string> rule in validatedRules)
                {
                    if (word.Contains(rule.Key))
                    {
                        word = word.Replace(rule.Key, rule.Value);

                        CheckUniqueRule(rule);                       

                        changed = true;

                        break;
                    }
                }
            }

            Console.WriteLine(word);
        }

        private static void CheckUniqueRule(KeyValuePair<string,string> rule)
        {
            if(rule.Value.StartsWith("."))
                validatedRules.Remove(rule.Key);
        }
    }
}
