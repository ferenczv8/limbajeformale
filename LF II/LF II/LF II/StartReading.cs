﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using 
namespace LF_II
{
    class StartReading
    {
        private List<char> _vocabulary;
        private Dictionary<string, string> _rules;
        private string word;
        public StartReading(){

            _rules = new Dictionary<string, string>();

            ReadInfo();

            ReadWord();
            
            ReadRules();
        }

        public List<char> Vocabulary
        {
            get { return _vocabulary ?? new List<char>(); }
        }
        public Dictionary<string, string> Rules
        {
            get { return _rules ?? new Dictionary<string, string>(); }
        }
        public string Word
        {
            get { return word; }
        }

        private void ReadInfo(){
           // Console.WriteLine("Give vocabulary:");

            string vocab = "a,x,y,#";//Console.ReadLine();
            
            _vocabulary = vocab.Split(',').Where(n => (n!=null) && n!= string.Empty)
                .Select(n => Convert.ToChar(n))
                .ToList();
        }
        private void ReadWord()
        {
            //Console.WriteLine("Give word:");
            word = "aa#aaa";//Console.ReadLine();
        }

        private Validate ValidR;
        private void ReadRules()
        {
            ValidR = new Validate();
            //Console.WriteLine("Say how many rules you want");
            //int nr = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Insert Rules");

            //int i = 0;
            //do
            //{
            //    var rule = ValidR.GetValues(Console.ReadLine());

            //    _rules.Add(rule[0], rule[1]);

            //    i++;
           
            //} while (i < nr);
            _rules.Add("ya", "ay");
            _rules.Add("xa", "ayx");
            _rules.Add("x", "@");
            _rules.Add("a#", "#x");
            _rules.Add("#a", "#");
            _rules.Add("#", "@");
            _rules.Add("y", "a");
        }
    }
}
