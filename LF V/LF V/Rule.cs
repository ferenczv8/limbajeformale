﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LF_IV
{
    class Rule
    {
        private string state;
        private Dictionary<string, List<string>> directions;

        public Rule(string s, Dictionary<string, List<string>> d)
        {
            this.State = s;
            this.Directions = d;
        }
        public Dictionary<string, List<string>> Directions
        {
            get { return directions; }
            set { directions = value; }
        }
        public string State
        {
            get { return state; }
            set { state = value; }
        }
    }
}
