﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LF_IV
{
    class Minimizare
    {
        private List<string> vocabulary;

        private List<Rule> nod;

        private string word;

        private string initialNode;
        private string finalNode;
        private bool failed = false;
        private int[][] Matrix;
        private int length;
        

        #region Properties

        public List<string> Vocabulary
        {
            get { return vocabulary; }
            set { vocabulary = value; }
        }

        public List<Rule> Nodes
        {
            get { return nod; }
            set { nod = value; }
        }

        public string Word
        {
            get { return word; }
            set { word = value; }
        }


        #endregion

        public Minimizare()
        {
            Initialize();
             
            ExecuteAlgorithm();
        }
        #region Init
        private void Initialize()
        {
            AssignVocabulary();

            AssignRules();

            AssignMatrix();

        }
        private void AssignMatrix()
        {
            length = Nodes.Count;

            Matrix = new int[length][];

            for (int i = 0; i < length; i++)
                Matrix[i] = new int[length];
        }

        private void AssignWord()
        {
            Word = Read.Storage[1].FirstOrDefault();
        }

        private void AssignVocabulary()
        {
            Vocabulary = new List<string>();
            Read.Storage[0].FirstOrDefault().ToCharArray().ToList<char>().ForEach(n =>
            {

                if (n != ',' && n != ' ')
                    Vocabulary.Add(n.ToString());
            });
        }

        private void AssignRules()
        {

            int poz = 0;
            nod = new List<Rule>();

            Read.Storage[1].ForEach(n =>
            {
                nod.Add(new Rule(AddState(n, poz++), AddDirections(n)));
            });
        }

        #endregion
        private Dictionary<string, List<string>> AddDirections(string s)
        {
            var dex = new Dictionary<string, List<string>>();

            var directions = Regex.Matches(s, @"\s*[{}\d,]+");
            
            for (int i = 1; i < directions.Count; i++)
            {
                dex.Add(Vocabulary[i-1], GetDirections(directions[i].Value));
            }

            return dex;
        }
        
        private List<string> GetDirections(string value)
        {
            return Regex.Matches(value, @"[0-9-]").Cast<Match>().Select(n => n.Value).ToList();
        }

        //poz - used to determine position of initialNode
        private string AddState(string element, int poz)
        {
            var state = Regex.Match(element, @"[a-z]+").Value;
            if(state == "initial")
            {
                initialNode = poz.ToString();
            }
            return state;
        }
        
        private void ExecuteAlgorithm()
        {

            FirstStep();

            SecondStep();

            ThirdStep();//establish which nodes are similar
        }

        //fill the matrix where the nodes differ one from another
        private void FirstStep()
        {
            for (int i = 0; i < length; i++)
               for (int j = i + 1; j < length; j++)
                   if ((nod[i].State == "final" || nod[j].State == "final") && (nod[i].State != nod[j].State))
                        Matrix[i][j] = Matrix[j][i] = 1;
                    else
                        Matrix[i][j] = Matrix[j][i] = 0;
            
        }

        //compare results of directions with existing matrix
        private void SecondStep()
        {
            Boolean modified = false;

            for (int i = 0; i < length; i++)
                for (int j = i + 1; j < length; j++)
                {
                    if (Matrix[j][i] == 0)
                    {
                        int a1 = Convert.ToInt32(nod[j].Directions[Vocabulary[0]][0]);
                        int a2 = Convert.ToInt32(nod[i].Directions[Vocabulary[0]][0]);

                        int b1 = Convert.ToInt32(nod[j].Directions[Vocabulary[1]][0]);
                        int b2 = Convert.ToInt32(nod[i].Directions[Vocabulary[1]][0]);

                        if (a1 != a2 && Matrix[a1][a2] == 1)
                        {
                            Matrix[i][j] = Matrix[j][i] = 1;
                            modified = true;
                        }
                        else if (b1 != b2 && Matrix[b1][b2] == 1)
                        {
                            Matrix[i][j] = Matrix[j][i] = 1;
                            modified = true;
                        }
                    }
                }
            if(modified)
            {
                SecondStep();//repeat until there are no modofications made
            }
        }

        private void ThirdStep()
        {
            int [] RangNod = new int[length];
            for (int i = 0; i < length; i++)
                RangNod[i] = 0;

            for (int i = 0; i < length; i++)
               for (int j = 0; j < length; j++)
                   if (i != j && Matrix[j][i] == 0 || Matrix[i][j] == 0)
                   {
                       RangNod[i]++;
                       RangNod[j]++;
                   }

            for (int i = 0; i < length; i++)
            {
                string text = string.Empty;
                if(RangNod[i]>2)
                for (int j = i+1; j < length; j++)
                {
                    if (RangNod[i] == RangNod[j] && i!=j)
                    {
                        text = text + " =q" + j;
                        RangNod[j] = 0;//sa nu mai fie luatin calcul pe viitor
                    }
                }
                if(RangNod[i]!=0)//pt o afisare ok
                Console.WriteLine(Environment.NewLine + "q" + i + text);
                
            }
        }

        private Boolean CheckResult(string currentNode)
        {
            
            if (nod[Convert.ToInt32(currentNode)].State == "final")
            {
                Console.WriteLine(currentNode + Environment.NewLine + "Correct it is a final state");
                return true;
            }

            return false;
              
        }
    }
}