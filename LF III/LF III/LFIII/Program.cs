﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LFIII
{
    public class Program
    {
        private static void StartAFD()
        {
            if (Validate()) //algorithm executes only if input is valid
            { AFD algorithm = new AFD(); } 

        }

        private static Boolean Validate()
        {
            Validate v = new Validate();

            v.Start();

            return !v.Failed;
        }
        public static void Main()
        {
            Read.StartReading();

            StartAFD();
        }
    }
}
