﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LFIII
{
    class Validate
    {
        private bool failed = false;

        public bool Failed
        {
          get { return failed; }
          set { failed = value; }
        }


        public void Start()
        {
            ValidateWord();

            ValidateRules();
        }

        //check to seee if word has all characters from the Vocabulary
        private void ValidateWord()
        {
            
            var letters = Read.Storage[1].FirstOrDefault().ToCharArray().ToList<char>();

            Failed = letters.Any(n =>{
                if (!Read.Storage[0].FirstOrDefault().Contains(n.ToString()))
                    return true;
                return false;
            });

            if (Failed == true)
                Console.WriteLine("Word is incorrect");             
        }

        
        private  void ValidateRules()
        {
            int count = Read.Storage[2].Count;
            Failed =  Read.Storage[2].Any(n =>
            {
                if (!CheckRule(n,count))
                    return true;
                return false;
            });
        }

        private  Boolean CheckRule(string s,int max)
        {
            //check format of input for rules
            if (!Regex.Match(s, @"[0-9-],\s*([0-9-]),\s*([0-9-])\s*[: a-z]*").Success)
                return false;

            var found = Regex.Matches(s, "[0-9-]");

            //Check for 3 elements in a rule
            if (found.Count != 3)
                return false;


            //check if there is a Nod that goes to an element that is not defined 
            foreach(Match m in found)
            {
               
                if (m.Value!= "-" && Convert.ToInt32(m.Value) > max)
                    return false;
            }

            return true;
        }
    }
}
