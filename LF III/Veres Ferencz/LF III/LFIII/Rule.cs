﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LFIII
{
    class Rule
    {
        private string state;
        private Dictionary<string, string> directions;

        public Rule(string s, Dictionary<string, string> d)
        {
            this.State = s;
            this.Directions = d;
        }
        public Dictionary<string, string> Directions
        {
            get { return directions; }
            set { directions = value; }
        }
        public string State
        {
            get { return state; }
            set { state = value; }
        }
    }
}
