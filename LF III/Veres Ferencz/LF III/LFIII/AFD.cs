﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LFIII
{
    class AFD
    {
        private List<string> vocabulary;

        private List<Rule> nod;

        private string word;

        private string initialNode;
        private string finalNode;
        private bool failed = false;

        #region Properties

        public List<string> Vocabulary
        {
            get { return vocabulary; }
            set { vocabulary = value; }
        }

        public List<Rule> Nodes
        {
            get { return nod; }
            set { nod = value; }
        }

        public string Word
        {
            get { return word; }
            set { word = value; }
        }


        #endregion

        public AFD()
        {
            Initialize();

            ExecuteAlgorithm();

            CheckResult();
        }
        #region Init
        private void Initialize()
        {
            AssignVocabulary();

            AssignWord();

            AssignRules();
        }

        private void AssignWord()
        {
            Word = Read.Storage[1].FirstOrDefault();
        }

        private void AssignVocabulary()
        {
            Vocabulary = new List<string>();
            Read.Storage[0].FirstOrDefault().ToCharArray().ToList<char>().ForEach(n =>
            {

                if (n != ',' && n != ' ')
                    Vocabulary.Add(n.ToString());
            });
        }

        private void AssignRules()
        {

            int poz = 0;
            nod = new List<Rule>();

            Read.Storage[2].ForEach(n =>
            {
                nod.Add(new Rule(AddState(n, poz++), AddDirections(n)));
            });
        }

        #endregion
        private Dictionary<string, string> AddDirections(string s)
        {

            Match m = Regex.Match(s, @",\s*(?<first>[0-9-]),\s*(?<second>[0-9-])");

            var dex = new Dictionary<string, string>();

            dex.Add(Vocabulary[0], m.Groups["first"].Value);
            dex.Add(Vocabulary[1], m.Groups["second"].Value);

            return dex;
        }

        //poz - used to determine position of initialNode
        private string AddState(string element, int poz)
        {
            var state = Regex.Match(element, @"[a-z]+").Value;
            if(state == "initial")
            {
                initialNode = poz.ToString();
            }
            return state;
        }
        private void ExecuteAlgorithm()
        {
            string currentNode = initialNode;
        
            foreach (var letter in Word.ToCharArray().ToList<Char>())
             {
                if (currentNode == "-")
                { failed = true; break; }
                currentNode = nod[Convert.ToInt32(currentNode)].Directions[letter.ToString()];
            }
            finalNode = currentNode;

        }

        private void CheckResult()
        {
            if(!failed)
                if (nod[Convert.ToInt32( finalNode) ].State == "final")
                {
                    Console.WriteLine(finalNode + "Correct it is a final state");
                }
                else
                {
                    Console.WriteLine(finalNode + "Wrong, not a final state");
                }
            else{
                Console.WriteLine("Failed to finish");
            }
        }
    }
}
