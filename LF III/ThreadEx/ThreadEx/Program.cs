﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ThreadEx
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Begin for" + Environment.NewLine);
            string [] s = new string[]{"a","b","c","d","e"};
  
            //for (int i = 0; i < 3; i++)
            //{
            //    Console.WriteLine(s[i]);

            //}
            Thread[] t = new  Thread[3];
            for (int i = 0; i < 3; i++)
            {
                //t[i] = new Thread(DoMain);
                t[i] = new Thread(new Used().Do);
               
                t[i].Start(s[i]);



            }
            
            foreach(var x in t)
            {
                x.Join();
            }
            Console.WriteLine("After Join");
            Console.ReadLine();
        }

        //static void DoMain(object s)
        //{
        //    Used u = new Used(s.ToString(),null);
        //    u.Do();
        //}
    }

    class Used
    {
        //public Used(string x, string name)
        //{
        //    //Console.WriteLine("Name = " + name +  x + Environment.NewLine);
        //}
        //public  void Do() { Console.WriteLine("Nothing"); Thread.Sleep(3000); }
        public void Do(object x) { Console.WriteLine(x + "Nothing"); Thread.Sleep(10000); Console.WriteLine("WokeUp"); }

    }
    //https://stackoverflow.com/questions/2177667/is-there-a-way-of-splitting-a-c-sharp-generic-dictionary-into-multiple-dictionar
    //https://stackoverflow.com/questions/419019/split-list-into-sublists-with-linq
    //http://resources.infosecinstitute.com/multithreading/
    //https://stackoverflow.com/questions/21295639/c-sharp-splitting-a-list-into-n-amount-of-sub-lists
}
